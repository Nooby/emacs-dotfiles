(require 'global-functions)
(ensure-package-installed 'evil)

(require 'evil)
(evil-mode t)

(provide 'init-evil)
